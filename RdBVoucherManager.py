#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import voucher_database
import sys
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QWidget,
    QAction,
    QHBoxLayout, QVBoxLayout,
    QPushButton, QLineEdit, QLabel,
    QMessageBox, QInputDialog
)
from PyQt5 import QtGui
from PyQt5.Qt import Qt
import logging
import voucher_A4generator


class RdBVoucherManager(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.widget = QWidget(self)
        self.setCentralWidget(self.widget)

        self.btn_sell = QPushButton("Marquer comme vendu")
        self.btn_sell.clicked.connect(self.buttonActionClicked)
        self.btn_sell.setFocusPolicy(Qt.TabFocus)
        self.btn_use = QPushButton("Marquer comme utilisé")
        self.btn_use.clicked.connect(self.buttonActionClicked)
        self.btn_use.setFocusPolicy(Qt.TabFocus)

        self.btn_destroy = QPushButton("Détruire")
        self.btn_destroy.clicked.connect(self.buttonDestroyClicked)
        self.btn_destroy.setFocusPolicy(Qt.NoFocus)

        self.field_barcode = QLineEdit()
        self.field_barcode.textChanged[str].connect(self.update_barcode_status)
        self.field_barcode.setClearButtonEnabled(True)

        self.lbl_barcode = QLabel()

        img_barcode = QLabel()
        img_barcode.setPixmap(QtGui.QPixmap('barcode.png'))

        vbox_barcode = QVBoxLayout()
        vbox_barcode.addStretch(1)
        vbox_barcode.addWidget(img_barcode)
        vbox_barcode.addWidget(self.field_barcode)
        vbox_barcode.addWidget(self.lbl_barcode)
        vbox_barcode.addStretch(1)

        hbox_barcode = QHBoxLayout()
        hbox_barcode.addStretch(1)
        hbox_barcode.addLayout(vbox_barcode)
        hbox_barcode.addStretch(1)

        hbox_btn = QHBoxLayout()
        hbox_btn.addStretch(1)
        hbox_btn.addWidget(self.btn_destroy)
        hbox_btn.addStretch(1)
        hbox_btn.addWidget(self.btn_sell)
        hbox_btn.addWidget(self.btn_use)
        hbox_btn.addStretch(1)

        vbox_all = QVBoxLayout()
        vbox_all.addStretch(1)
        vbox_all.addLayout(hbox_barcode)
        vbox_all.addLayout(hbox_btn)
        vbox_all.addStretch(1)

        self.widget.setLayout(vbox_all)

        self.init_menuBar()

        self.statusBar()

        self.setWindowTitle("Gestionnaire de bons d'achat")
        self.update_barcode_status()
        self.show()

    def init_menuBar(self):
        generateAct = QAction('&Générer en A4', self)
        generateAct.setStatusTip("Générer 6 bons d'achat")
        generateAct.triggered.connect(self.generate_vouchers)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&Générer')
        fileMenu.addAction(generateAct)

    def is_valid_barcode(self, barcode):
        if len(barcode) != 8:
            return False
        if not barcode.isdigit():
            return False
        return True

    def update_barcode_status(self):
        self.btn_destroy.setEnabled(False)
        self.btn_destroy.setStyleSheet("background-color:tomato; color:grey;")
        self.btn_sell.setEnabled(False)
        self.btn_use.setEnabled(False)
        if len(self.field_barcode.text()) == 0:
            # Field empty
            self.lbl_barcode.setText("Scannez un code barre")
            self.field_barcode.setStyleSheet("")
        elif not self.is_valid_barcode(self.field_barcode.text()):
            # Malformed barcode
            self.lbl_barcode.setText("Ceci n'est pas un code barre valide")
            self.field_barcode.setStyleSheet("background-color:tomato; color:black;")
        else:
            # Good barcode, check if it is a voucher
            valid = True
            try:
                vid, value = voucher_database.from_barcode(self.field_barcode.text())
            except ValueError:
                valid = False
            valid = valid and voucher_database.is_valid_voucher(vid, value)
            if not valid:
                self.lbl_barcode.setText("Ce bon d'achat n'existe pas.<br />"
                                         "Soit:<br />"
                                         " - La saisie est incorrecte<br />"
                                         " - C'est peut-être un faux !")
                self.field_barcode.setStyleSheet("background-color:red; color:white;")
            else:
                state = voucher_database.get_voucher_state(vid)
                if state == voucher_database.VoucherState.destroyed:
                    self.lbl_barcode.setText("Ce bon d'achat a été détruit.")
                    self.field_barcode.setStyleSheet("background-color:black; color:white;")
                else:
                    self.btn_destroy.setEnabled(True)
                    self.btn_destroy.setStyleSheet("background-color:red; color:white;")
                    if state == voucher_database.VoucherState.used:
                        self.field_barcode.setStyleSheet("background-color:darkgreen; color:white;")
                        self.lbl_barcode.setText("Ce bon d'achat a été utilisé comme<br />"
                                                 "payement, il n'est plus valable.<br />"
                                                 "Il avait une valeur de %i€" % value)

                    elif state == voucher_database.VoucherState.generated:
                        self.btn_sell.setEnabled(True)
                        self.field_barcode.setStyleSheet("background-color:limegreen; color:black;")
                        self.lbl_barcode.setText("Ce bon d'achat est valide<br />"
                                                 "et disponible à la vente.<br />"
                                                 "Il a une valeur de %i€" % value)
                    elif state == voucher_database.VoucherState.sold:
                        self.btn_use.setEnabled(True)
                        self.field_barcode.setStyleSheet("background-color:green; color:black;")
                        self.lbl_barcode.setText("Ce bon d'achat a été vendu.<br />"
                                                 "Il peut être utilisé pour payer.<br />"
                                                 "Il a une valeur de %i€" % value)

    def generate_vouchers(self):
        value, ok = QInputDialog.getInt(self, "Génération de 6 bons d'achat",
                                        "Entrez la valeur des bons d'achat:",
                                        min=0, max=99)
        if not ok:
            return
        vouchers_file, voucher_ids = voucher_A4generator.generate_6_vouchers_on_A4(value)
        print(vouchers_file.name)
        voucher_A4generator.open_file(vouchers_file.name)
        # dialog to confirm print or cancel
        printed = QMessageBox.question(self,
                                       "Confirmer l'impression",
                                       "Avez vous bien imprimé les bons d'achat ?",
                                       QMessageBox.Yes | QMessageBox.No)
        if printed == QMessageBox.No:
            for i in voucher_ids:
                voucher_database.destroy_voucher(i)
        vouchers_file.close()

    def buttonClicked(self):
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + " was pressed")

    def buttonDestroyClicked(self):
        self.buttonClicked()
        voucher_barcode = self.field_barcode.text()
        box_confirm = QMessageBox()
        box_confirm.setText("Détruire le bon d'achat %s?" % voucher_barcode)
        box_confirm.setInformativeText("ATTENTION, Cette action est irréversible!")
        box_confirm.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        box_confirm.setDefaultButton(QMessageBox.Cancel)
        response = box_confirm.exec()
        if response == QMessageBox.Ok:
            vid, value = voucher_database.from_barcode(voucher_barcode)
            logging.warning("Destroying %s (%i, %i)", voucher_barcode, vid, value)
            voucher_database.destroy_voucher(vid)
        self.update_barcode_status()

    def buttonActionClicked(self):
        vid, value = voucher_database.from_barcode(self.field_barcode.text())
        voucher_database.update_voucher(vid)
        self.update_barcode_status()



if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = RdBVoucherManager()
    sys.exit(app.exec_())
