#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import enum
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, Enum
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
import logging
from simple_id_obfuscator import SimpleIDObfuscator as SIO
import config


obfuscator = SIO(config.password)


db = create_engine('sqlite:///' + config.database)
Base = declarative_base()


class VoucherState(enum.Enum):
    generated = 0  # Voucher has been generated/printed
    sold = 1       # Voucher has been sold to somebody
    used = 2       # Voucher has been used for payment
    destroyed = 3  # Voucher is invalid


class Voucher(Base):
    __tablename__ = 'vouchers'
    id = Column(Integer, primary_key=True)
    value = Column(Integer)
    state = Column(Enum(VoucherState))

    def __init__(self, value):
        self.value = value
        self.state = VoucherState.generated

    def set_sold(self):
        if self.state == VoucherState.new:
            self.state = VoucherState.sold
        else:
            raise ValueError

    def set_used(self):
        if self.state == VoucherState.sold:
            self.state = VoucherState.used
        else:
            raise ValueError


Base.metadata.create_all(db)
session = Session(db)


def get_voucher(id):
    voucher = None
    try:
        voucher = session.query(Voucher).filter_by(id=id).one()
    except SQLAlchemyError:
        logging.warning("Impossible de trouver le bon d'achat %i", id)
    return voucher


def get_voucher_state(id):
    return get_voucher(id).state


def is_valid_voucher(id, value):
    voucher = get_voucher(id)
    if not voucher:
        return False
    if voucher.value != value:
        logging.warning("Le bon %i a été enregistré avec une valeur de %i€ et non pas %i€", id, voucher.value, value)
        return False
    return True


def generate_voucher(value):
    try:
        new_voucher = Voucher(value=value)
        session.add(new_voucher)
        session.commit()
        logging.info("Nouveau bon d'achat %i de %i€ (%s)",
                     new_voucher.id, new_voucher.value, to_barcode(new_voucher.id, new_voucher.value))
    except SQLAlchemyError as e:
        logging.warning("Impossible de créer un nouveau bon d'achat", e.msg)
        new_voucher = None
        session.rollback()
    return new_voucher


def destroy_voucher(id):
    voucher = get_voucher(id)
    try:
        voucher.state = VoucherState.destroyed
        session.commit()
    except SQLAlchemyError:
        logging.warning("Impossible de détruire le bon d'achat %i", id)
        session.rollback()


def update_voucher(id):
    voucher = get_voucher(id)
    try:
        if voucher.state == VoucherState.generated:
            voucher.state = VoucherState.sold
        elif voucher.state == VoucherState.sold:
            voucher.state = VoucherState.used
        else:
            logging.warning("État invalide pour le bon %i: %i", id, voucher.state)
            session.rollback()
            return
        session.commit()
    except SQLAlchemyError:
        logging.warning("Impossible de mettre à jour le bon d'achat %i", id)
        session.rollback()


def fix_ean8_checksum(barcode):
    sum = 0
    weight = 3
    for x in barcode:
        sum += int(x) * weight
        if weight == 3:
            weight = 1
        else:
            weight = 3
    checksum = (10 - (sum % 10)) % 10
    return barcode + str(checksum)


def from_barcode(barcode):
    if fix_ean8_checksum(barcode[:-1]) != barcode:
        raise ValueError("Checksum control failed for {}".format(barcode))
    tmp_obfuscated = obfuscator.transform(int(int(barcode) / 10))
    id_value = str(tmp_obfuscated).rjust(7, '0')
    id, value = int(id_value[:5]), int(id_value[5:])
    return id, value


def to_barcode(vid, value):
    tmp_obfuscated = obfuscator.transform((int(vid) * 100 + int(value)))
    barcode = fix_ean8_checksum(str(tmp_obfuscated).rjust(7, '0'))
    return barcode
