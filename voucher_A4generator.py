#!/usr/bin/env python
# coding: utf-8
# docs: http://effbot.org/zone/element-namespaces.htm
#       https://stackabuse.com/reading-and-writing-xml-files-in-python/

import xml.etree.ElementTree as ET
import shutil
import barcode
import voucher_database
from io import BytesIO
import os
import subprocess
import sys
from cairosvg import svg2pdf
import tempfile

import time

SVG_NAMESPACE = {'dc'      : "http://purl.org/dc/elements/1.1/",
                 'cc'      : "http://creativecommons.org/ns#",
                 'rdf'     : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                 'svg'     : "http://www.w3.org/2000/svg",
                 ''        : "http://www.w3.org/2000/svg",
                 'xlink'   : "http://www.w3.org/1999/xlink",
                 'sodipodi': "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd",
                 'inkscape': "http://www.inkscape.org/namespaces/inkscape"}

for prefix, uri in SVG_NAMESPACE.items():
    ET.register_namespace(prefix, uri)


class BarcodeSVG:
    def __init__(self, svg_file=None, svg_content=None):
        """
        svg_file: Barcode SVG file generated with python-barcode
        svg_content: same but as str
        """
        if svg_file:
            tree = ET.parse(svg_file)
        else:
            tree = ET.fromstring(svg_content)
        self.group = tree.find('svg:g', SVG_NAMESPACE)
        self.clean()

    def clean(self):
        """ Remove the white background
        """
        for elt in self.group.findall('./', SVG_NAMESPACE):
            try:
                if elt.attrib['height'] == '100%':
                    self.group.remove(elt)
                    continue
            except KeyError:
                continue  # No height attribute, skip

    def move(self, mov_x, mov_y):
        for elt in self.group.findall('./', SVG_NAMESPACE):
            if elt.tag == '{http://www.w3.org/2000/svg}rect':
                elt.set('height', '14.000mm')
            x = float(elt.attrib['x'].rstrip('m'))
            y = float(elt.attrib['y'].rstrip('m'))
            elt.set('x', '{}mm'.format(x + mov_x))
            if elt.tag == '{http://www.w3.org/2000/svg}text':
                elt.set('y', '{}mm'.format(y + mov_y - 3))
            else:
                elt.set('y', '{}mm'.format(y + mov_y))

    def add_price(self, pos_x, pos_y, value):
        price = ET.SubElement(self.group, 'text')
        price.set('style', "fill:black;font-size:20pt;text-anchor:middle;")
        price.set('x', '{}mm'.format(pos_x))
        price.set('y', '{}mm'.format(pos_y))
        price.text = '{}€'.format(value)
        # price.text = '{}&#8364;'.format(value)


def generate_voucher_svg(fp, value):
    v = voucher_database.generate_voucher(value)
    voucher_barcode = voucher_database.to_barcode(v.id, v.value)
    print(voucher_barcode)
    barcode.generate('EAN8', voucher_barcode, output=fp)
    return v.id


def generate_6_vouchers_on_A4(value):
    generated_voucher_ids = []
    with tempfile.NamedTemporaryFile(suffix='.svg') as vouchers_svg_file:
        vouchers_svg_filepath = vouchers_svg_file.name
        print(vouchers_svg_filepath)
        template_filepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'template_A4.svg')
        print(template_filepath)
        shutil.copy(template_filepath, vouchers_svg_filepath)
        vouchers_tree = ET.parse(vouchers_svg_file)
        vouchers_root = vouchers_tree.getroot()
        for (mov_x, mov_y, x, y) in [(112.5, 46, 80, 60), (112.5, 113.3, 80, 127.3), (112.5, 180.5, 80, 195),
                                     (260, 46, 228, 60), (260, 113.3, 228, 127.3), (260, 180.5, 228, 195)]:
            fp = BytesIO()
            voucher_id = generate_voucher_svg(fp, value)
            generated_voucher_ids.append(voucher_id)
            byte_str = fp.getvalue()
            svg_content = byte_str.decode('UTF-8')
            barcode = BarcodeSVG(svg_content=svg_content)
            barcode.move(mov_x, mov_y)
            barcode.add_price(x, y, value)
            vouchers_root.append(barcode.group)
        vouchers_tree.write(vouchers_svg_filepath)
        tmp_output = tempfile.NamedTemporaryFile(suffix='.pdf')
        svg2pdf(url=vouchers_svg_filepath, write_to=tmp_output.name)
        return tmp_output, generated_voucher_ids


def open_file(filepath):
    if sys.platform.startswith('darwin'):
        subprocess.check_call(('open', filepath))
    elif os.name == 'nt': # For Windows
        os.startfile(filepath)
    elif os.name == 'posix': # For Linux, Mac, etc.
        subprocess.check_call(('xdg-open', filepath))


if __name__ == '__main__':
    vouchers_file, v_ids = generate_6_vouchers_on_A4(42)
    open_file(vouchers_file.name)
    time.sleep(10)
    vouchers_file.close()
