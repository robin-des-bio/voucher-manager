# RdB Voucher Manager #

Gui application to manage giftable vouchers.

## Requirements
`pip install -r requirements.txt`

## Credits
Obfuscation functions are inspired from https://github.com/jidn/obscure
