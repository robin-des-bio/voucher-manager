#!/bin/env python3
# coding: utf-8


import binascii
import copy


class SimpleIDObfuscator:
    def __init__(self, password, prime=0xc101):
        self.prime = prime
        self.salt = self._pass_to_salt(password)

    def _pass_to_salt(self, password):
        """Converts a str to its hexadecimal/int representation.
        """
        return int(binascii.hexlify(password.encode('utf-8')), 16)

    def _mangle11(self, x):
        """Produce an 11-bit mangled value based off a 11-bit integer.
        The feistel cipher uses a function each round to mangle the
        input bits in a way that does not have to be invertable.
        Args:
           x (integer): 13-bit integer
        Returns:
           The output of the Feistel Cipher's F(x)
        """
        """
        :param i: 13-bit integer
        :returns: 13-bit transformation.
        """
        x = (self.salt ^ x) * self.prime
        return x >> (x & 0xf) & 0x7ff

    def transform(self, i):
        """Reversibly transform a 22-bit integer using Feistel cipher.
        Args:
            i (integer): number to obscure/unobscure
        Returns:
            The number transformed or restored.
        """
        """
        :param i: integer
        :returns: transformed integer so transform(transform(i)) == i
        """
        l = i & 0x7ff
        h = i >> 11 & 0x7ff ^ self._mangle11(l)
        return ((l ^ self._mangle11(h)) << 11) + h


if __name__ == "__main__":

    def _test_unique_transformation():
        sio = SimpleIDObfuscator("test")
        for x in range(10000000):
            if 10000 > x or x > 0x400000:
                continue
            y = sio.transform(x)
            if x != sio.transform(y):
                print("Error", x, y)

    def _test_bruteforce():
        print("Starting bruteforce test")
        sio = SimpleIDObfuscator("vivent les pandas")
        tmp_sio = copy.deepcopy(sio)
        reference_value = sio.transform(42)
        for x in range(sio.salt * 2):
            tmp_sio.salt = x
            if tmp_sio.transform(42) == reference_value:
                print("Warning: salt", tmp_sio.salt,
                      "does the same transformation as salt", sio.salt)
                print(sio.transform(42), tmp_sio.transform(42), tmp_sio.transform(reference_value))
                count = 0
                for y in range(1, 10000):
                    for z in range(10, 100, 10):
                        num = y * 100 + z
                        if tmp_sio.transform(num) == sio.transform(num):
                            count += 1
                print(count)

    _test_unique_transformation()
    _test_bruteforce()
